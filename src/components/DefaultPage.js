import { PureComponent } from 'react'
import { Route, Routes } from 'react-router-dom'
import routes from '../routes'
import Footer from './footer/Footer'
import Header from './header/Header'

export default class DefaultPage extends PureComponent {
    render() {
        return (
            <div className="default-container">
                <Header />
                <main>
                    <Routes>
                        {
                            routes.map(route =>{
                                return(
                                    <Route key={route.path} path={route.path} element={<route.component />}></Route>
                                )
                            })
                        }
                    </Routes>
                </main>
                <Footer />
            </div>
        )
    }
}
