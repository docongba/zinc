import { PureComponent } from 'react'

export default class Header extends PureComponent {
    render() {
        return (
            <div className="header-container">
                <div className="header-content">
                    <div className="left-container">
                        <div className="ic-logo-name">
                            <i className="ic-zinc" />
                            <div className="logo-name">Zinc</div>
                        </div>
                        <div className="link-page">Auctions</div>
                        <div className="link-page">Auctions</div>
                        <div className="link-page">FAQ</div>
                        <div className="link-page">Feedback</div>
                    </div>
                    <div className="right-container">
                        <div className="btn-connect-wallet">Connet Wallet</div>
                    </div>
                </div>
            </div>
        )
    }
}
