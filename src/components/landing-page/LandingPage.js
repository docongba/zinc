import React, { PureComponent } from 'react';
import firstimage from '../../assets/images/firstimage.png';
import secondimage from '../../assets/images/secondimage.png';

export default class LandingPage extends PureComponent {
    render() {
        return (
            <div className="landing-page-container">
                <div className="first-content">
                    <div className="left-container">
                        <div className="first-phase">
                            A platform for open, transparent, and user-friendly token Token Launch Auctions, powered by Balancer Liquidity Bootstrapping Pools.
                        </div>
                        <div className="btn-common black-color explore-btn">Explore Auctions</div>
                        <div className="btn-common gray-color read-more-btn">Read more</div>
                    </div>
                    <div className="right-container">
                        <img
                            className="img-common"
                            src={firstimage}
                        />
                    </div>
                </div>
                <div className="second-content">
                    <div className="title">
                        What is Zinс?
                    </div>
                    <div className="wrapper-second-content">
                        <div className="left-container">
                            <img
                            className="img-common"
                                src={secondimage}
                            />
                        </div>
                        <div className="right-container">
                            <div className="second-phase">
                                Zinc is the most open, transparent, and user-friendly way to participate in a Token Launch Auction (TLA).
                            </div>
                            <div className="third-phase">
                                An TLA is a simple crowdfunding mechanism that enables projects and ideas from across the world to raise money from individuals without barriers to entry
                            </div>
                        </div>
                    </div>
                </div>
                <div className="third-content">
                    <div className="left-container">
                        <div className="four-phase">
                            Referral system
                        </div>
                        <div className="five-phase">
                            Referral system to enable us to use specific link tracking and reward those referrers with a % of the money raised frmo them
                        </div>
                    </div>
                    <div className="right-container">

                        <img
                            className="img-common"
                            src={secondimage}
                        />
                    </div>
                </div>
                <div className="four-content">
                        <div className="left-container">
                            <img
                                className="img-common"
                                src={secondimage}
                            />
                        </div>
                        <div className="right-container">
                            <div className="six-phase">
                                GAMESTA support
                            </div>
                            <div className="seven-phase">
                                {` Authorised by GAMESTA support, so the auctions are not just random open source adds (on copper any can add a launch). We'll do this by vetting the projects before setting them live. So will need an intefeace for projecst that put their project details in, and then wait 24-72 hours as we vet them`}
                            </div>
                        </div>
                </div>
            </div>
        )
    }
}
