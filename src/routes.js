import LandingPage from "./components/landing-page/LandingPage";
import { PAGE_CONST } from "./utils/constants";

const routes = [
    { path: PAGE_CONST.ROUTE_LANDING_PAGE, component: LandingPage }
]

export default routes;