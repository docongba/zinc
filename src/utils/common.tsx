// input param : <dataJson : JSON File>, <keyStr   : input string>
export const t = (dataJson: Record<string, string>, keyStr: string) => {
    const keys = Object.keys(dataJson);
    const values = Object.values(dataJson);
    let checkflag = false;
    let valueString = '';
    keys.forEach(function (item, index) {
        if (item === keyStr) {
            checkflag = true;
            valueString = values[index];
        }
    });

    if (!checkflag) console.log(keyStr, ': the value does not exist');
    return valueString;
};
