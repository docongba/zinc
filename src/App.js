import { PureComponent } from 'react';
import {  Routes, Route } from 'react-router-dom';
import DefaultPage from './components/DefaultPage';

export default class App extends PureComponent {
    render() {
        return (
             <Routes>
                <Route path="/*" element={<DefaultPage />}></Route>
            </Routes>
        )
    }
}
